alter table ganashridb.ossn_users add column bhamashah_family_id char(7);
alter table ganashridb.ossn_users add column mobile_no bigint(11);
alter table ganashridb.ossn_users add column ifsc_code char(11);
alter table ganashridb.ossn_users add column account_no bigint(20);
alter table ganashridb.ossn_users add column aadhar_id bigint(15);

create table ganashridb.ossn_providers(
              guid      bigint(20) NOT NULL AUTO_INCREMENT,
              orgtype   varchar(25) NOT NULL,
              nameorg   text NOT NULL,
              purposeorg  TEXT NOT NULL,
              website   TEXT NOT NULL,
              contactname TEXT NOT NULL,
              corraddress TEXT NOT NULL,
              username   TEXT NOT NULL,
              email     TEXT NOT NULL,
              password  TEXT NOT NULL,
              salt      VARCHAR(8) NOT NULL,
              last_login  INT(11) NOT NULL,
              last_activity INT(11) NOT NULL,
              time_created  INT(11) NOT NULL,
              activation  TEXT,
              PRIMARY KEY (`guid`),
              KEY `last_login` (`last_login`),
              KEY `last_activity` (`last_activity`),
              KEY `time_created` (`time_created`),
              FULLTEXT KEY `orgtype` (`orgtype`),
              FULLTEXT KEY `email` (`email`),
              FULLTEXT KEY `nameorg` (`nameorg`)
  );

alter table ganashridb.ossn_providers Auto_increment = 1000;

CREATE TABLE `ganashridb.ossn_books` (
  `book_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `book_from` bigint(20) NOT NULL,
  `book_to` text NOT NULL,
  `time` int(11) NOT NULL,
  PRIMARY KEY (`book_id`),
  KEY `book_from` (`book_from`),
  KEY `time` (`time`)
);
