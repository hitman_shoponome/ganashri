<?php
/**
 * Open Source Social Network
 *
 * @package   (softlab24.com).ossn
 * @author    OSSN Core Team <info@softlab24.com>
 * @copyright 2014-2016 SOFTLAB24 LIMITED
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */

//error_log("In provider/register \r\n", 3, "/tmp/ossn_error.log");
header('Content-Type: application/json');
//error_log("In provider/register  ".input('nameorg')." Type of organisation is ".input('orgtype')." purpose of registration ".input('purposeorg')." website is ".input('website')." name of contact person ".input('contactname')." email of contact person ".input('email')." contact number of contact person ".input('contactnumber')." correspondence address  ".input('corraddress')." username  ".input('username')." password  ".input('password')."\r\n", 3, "/tmp/ossn_error.log");
$user['orgtype'] = input('orgtype');
$user['nameorg'] = input('nameorg');
$user['purposeorg'] = input('purposeorg');
$user['website'] = input('website');
$user['contactname'] = input('contactname');
$user['email'] = input('email');
$user['contactnumber'] = input('contactnumber');
$user['corraddress'] = input('corraddress');
$user['username'] = input('username');
$user['password'] = input('password');
// $fields = ossn_user_fields_names();
// foreach($fields['required'] as $field){
// 	$user[$field] = input($field);
// }

//error_log("checking data values \r\n", 3, "/tmp/ossn_error.log");
if (!empty($user)) {
    foreach ($user as $field => $value) {
        if (empty($value)) {
						//error_log("empty value\r\n", 3, "/tmp/ossn_error.log");
            $json['error'] = '1';
        }
    }
}

if (isset($json['error']) && !empty($json['error'])) {
    echo json_encode($json);
    exit;
}


$add = new OssnProvider;
//error_log("data values checked error is ".$json['error']." \r\n", 3, "/tmp/ossn_error.log");
$add->orgtype = $user['orgtype'];
$add->nameorg = $user['nameorg'];
$add->purposeorg = $user['purposeorg'];
$add->website = $user['website'];
$add->contactname = $user['contactname'];
$add->email = $user['email'];
$add->contactnumber = $user['contactnumber'];
$add->corraddress = $user['corraddress'];
$add->username = $user['username'];
$add->password = $user['password'];
//error_log("add created where orgtype is ".$add->orgtype." \r\n", 3, "/tmp/ossn_error.log");

//to implement checks for email nameorg etc.
//Later - anshul
// foreach($fields as $items){
// 	foreach($items as $field){
// 		$add->{$field} = $user[$field];
// 	}
// }

if (!$add->isUsername($user['username'])) {
    $em['dataerr'] = ossn_print('username:error');
    echo json_encode($em);
    exit;
}
if (!$add->isPassword()) {
    $em['dataerr'] = ossn_print('password:error');
    echo json_encode($em);
    exit;
}
//error_log("In register.php before isOssnUsername \r\n", 3, "/tmp/ossn_error.log");
if($add->isOssnUsername()){
    $em['dataerr'] = ossn_print('username:inuse');
    echo json_encode($em);
    exit;
}

if($add->isOssnEmail()){
    $em['dataerr'] = ossn_print('email:inuse');
    echo json_encode($em);
    exit;
}

//check if email is valid email
if(!$add->isEmail()){
    $em['dataerr'] = ossn_print('email:invalid');
    echo json_encode($em);
    exit;
}

//error_log("In before add provider \r\n", 3, "/tmp/ossn_error.log");
if ($add->addProvider()) {
    //error_log("Add provider successful \r\n", 3, "/tmp/ossn_error.log");
    $em['success'] = 1;
    $em['datasuccess'] = ossn_print('account:created:email');
    echo json_encode($em);
    exit;
} else {
    $em['dataerr'] = ossn_print('account:create:error:admin');
    echo json_encode($em);
    exit;
}
