<?php
/**
 * Open Source Social Network
 *
 * @package   (softlab24.com).ossn
 * @author    OSSN Core Team <info@softlab24.com>
 * @copyright 2014-2016 SOFTLAB24 LIMITED
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */

//error_log("\r\n==>\r\nIn Login\r\n", 3, '/tmp/ossn_error.log');

if(ossn_isLoggedin()) {
		redirect('home');
}
$bhamashah_id = strtoupper(input('bhamashah_id'));
$password = input('password');

if(empty($bhamashah_id) || empty($password)) {
		ossn_trigger_message(ossn_print('login:error'));
		redirect();
}

$user = ossn_user_by_bhamashah_id($bhamashah_id);

//error_log("Getting user by Bid \r\n", 3, '/tmp/ossn_error.log');

if($user && !$user->isUserVALIDATED()) {
		//error_log("Getting user validation \r\n", 3, '/tmp/ossn_error.log');
		$user->resendValidationEmail();
		ossn_trigger_message(ossn_print('ossn:user:validation:resend'), 'error');
		redirect(REF);
}
$vars = array(
		'user' => $user
);
//error_log("back to login.php line 38 vars is ".$vars."\r\n", 3, "/tmp/ossn_error.log");
ossn_trigger_callback('user', 'before:login', $vars);

$login           = new OssnUser;
$login->bhamashah_id = $bhamashah_id;
$login->password = $password;
if($login->Login()) {
		//One uneeded redirection when login #516
		ossn_trigger_callback('login', 'success', $vars);
		redirect('home');
} else {
		//error_log("In error section \r\n", 3, "/tmp/ossn_error.log");
		redirect('login?error=1');
}
