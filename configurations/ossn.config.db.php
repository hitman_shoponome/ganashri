<?php
/**
 * Open Source Social Network
 *
 * @package   (softlab24.com).ossn
 * @author    OSSN Core Team <info@softlab24.com>
 * @copyright 2014-2016 SOFTLAB24 LIMITED
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */


// replace localhost with your database host name;
$Ossn->host = 'localhost';

// replace ossnuser with your database username;
$Ossn->user = 'ossnuser';

// replace homelove9 with your database password;
$Ossn->password = 'homelove9';

// replace ossndb with your database name;
$Ossn->database = 'ossndb';