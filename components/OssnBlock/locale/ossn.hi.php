<?php
/**
 * Open Source Social Network
 *
 * @packageOpen Source Social Network
 * @author    Open Social Website Core Team <info@informatikon.com>
 * @copyright 2014-2016 SOFTLAB24 LIMITED
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */
$en = array(
    'user:blocked' => 'उपयोगकर्ता अवरुद्ध कर दिया गया है!',
    'user:block:error' => 'उपयोगकर्ता को ब्लॉक नहीं किया जा सकता! बाद में पुन: प्रयास करें।',
    'user:block' => 'ब्लॉक',
    'user:unblock' => 'अनब्लॉक',
    'user:unblocked' => 'उपयोगकर्ता को अवरुद्ध कर दिया गया है',
    'user:unblock:error' => 'उपयोगकर्ता को अनवरोधित नहीं किया जा सकता',
    'ossn:blocked:error' => 'अवरुद्ध',
    'ossn:blocked:error:note' => 'आप इस पृष्ठ को नहीं देख सकते हैं क्योंकि आपको उपयोगकर्ता द्वारा अवरुद्ध कर दिया गया है।',
);
ossn_register_languages('hi', $en);
