<?php
/**
 * Open Source Social Network
 *
 * @packageOpen Source Social Network
 * @author    Open Social Website Core Team <info@informatikon.com>
 * @copyright 2014-2016 SOFTLAB24 LIMITED
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */
$en = array(
		'ossnwall' => 'वॉल',
		'post:created' => 'पोस्ट सफलतापूर्वक बनाया!',
		'post:create:error' => 'पोस्ट नहीं बना सकता! बाद में पुन: प्रयास करें।',
		'post' => 'पोस्ट',
		'enter:location' => 'स्थान दर्ज करें',
		'tag:friends' => 'टैग मित्र',
		'wall:post:container' => "आपके मन में क्या है?",
		'post' => 'पोस्ट',
		'post:view' => 'पोस्ट देखें',
		'ossn:post:delete' => 'हटाएं',
		'post:delete:fail' => 'पोस्ट हटा नहीं सकते! बाद में पुन: प्रयास करें।',
		'post:delete:success' => 'पोस्ट सफलतापूर्वक हटाई गई!',
		'post:select:privacy' => 'कृपया पोस्ट के लिए गोपनीयता का चयन करें',
		'ossn:wall:settings:save:error' => 'सेटिंग सहेजी नहीं जा सकतीं! बाद में पुन: प्रयास करें।',
		'ossn:wall:settings:saved' => 'सेटिंग्स सहेजी गईं!',
		'ossn:wall:admin:notice' => 'होमपेज पोस्ट',
		'ossn:wall:allsite:posts' => 'सभी साइट पोस्ट',
		'ossn:wall:friends:posts' => 'केवल मित्र पोस्ट',
		'ossn:wall:post:saved' => 'पोस्ट सफलतापूर्वक सहेजा गया',
		'ossn:wall:post:save:error' => 'पोस्ट सहेज नहीं सकते'
);
ossn_register_languages('hi', $en);
