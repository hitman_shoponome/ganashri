<?php
/**
 * Open Source Social Network
 *
 * @packageOpen Source Social Network
 * @author    Open Social Website Core Team <info@informatikon.com>
 * @copyright 2014-2016 SOFTLAB24 LIMITED
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */
$en = array(
    'ossnads' => 'विज्ञापन प्रबंधक',
    'fields:required' => 'सभी फ़ील्ड आवश्यक हैं!',
    'ad:created' => 'विज्ञापन बना दिया गया है!',
    'ad:create:fail' => 'विज्ञापन नहीं बना सकते!',
    'ad:title' => 'शीर्षक',
    'ad:site:url' => 'साइटुरल',
    'ad:desc' => 'विवरण',
    'ad:photo' => 'फ़ोटो',
    'ad:browse' => 'ब्राउज',
    'ad:clicks' => 'क्लिक',
    'sponsored' => 'प्रायोजित',
	'ad:deleted' => "%s के शीर्षक के साथ विज्ञापन को सफलतापूर्वक हटा दिया गया है।",
	'ad:delete:fail' => 'विज्ञापन हटा नहीं सकते! बाद में पुन: प्रयास करें।',
	'ad:edited' => 'विज्ञापन सफलतापूर्वक संशोधित किया गया।',
	'ad:edit:fail' => 'विज्ञापन संपादित नहीं किया जा सकता! बाद में पुन: प्रयास करें।',
);
ossn_register_languages('hi', $en);
