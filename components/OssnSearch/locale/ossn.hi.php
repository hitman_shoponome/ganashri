<?php
/**
 * Open Source Social Network
 *
 * @package   (softlab24.com).ossn
 * @author    OSSN Core Team <info@softlab24.com>
 * @copyright 2014-2016 SOFTLAB24 LIMITED
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */

$en = array(
	'ossn:search' => 'खोज',
	'result:type' => 'परिणाम प्रकार',
	'search:result' => '%s के लिए खोज परिणाम',
	'ossn:search:topbar:search' => 'खोज समूहों, मित्रों और अधिक खोजें।',
	'ossn:search:no:result' => 'कोई परिणाम नहीं मिला!',
);
ossn_register_languages('hi', $en);
