<?php
/**
 * Open Source Social Network
 *
 * @packageOpen Source Social Network
 * @author    Open Social Website Core Team <info@informatikon.com>
 * @copyright 2014-2016 SOFTLAB24 LIMITED
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */
$en = array(
    'ossn:notifications:ossnpoke:poke' => "%s ने आपको पोक कर दिया है!",
    'user:poked' => 'आपने %s को पोक कर दिया है!',
    'user:poke:error' => '%s को पोक नहीं कर सकते! बाद में पुन: प्रयास करें.',
    'poke' => 'पोक',
);
ossn_register_languages('hi', $en);
