<?php
/**
 * Open Source Social Network
 *
 * @packageOpen Source Social Network
 * @author    Open Social Website Core Team <info@informatikon.com>
 * @copyright 2014-2016 SOFTLAB24 LIMITED
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */
$en = array(
    'ossnsitepages' => 'साइट पेज',
    'site:privacy' => 'गोपनीयता',
    'site:about' => 'बारे में',
    'site:terms' => 'नियम और शर्तें',

    'page:saved' => 'पृष्ठ सफलतापूर्वक सहेजा गया!',
    'page:save:error' => 'पृष्ठ सहेजा नहीं जा सकता! बाद में पुन: प्रयास करें।',

);
ossn_register_languages('hi', $en);
