<?php
/**
 * Open Source Social Network
 *
 * @package   (softlab24.com).ossn
 * @author    OSSN Core Team <info@softlab24.com>
 * @copyright 2014-2016 SOFTLAB24 LIMITED
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */

$en = array(
    'ossn:notifications:comments:post' => "%s ने पोस्ट पर टिप्पणी की।",
    'ossn:notifications:like:post' => "%s ने आपकी पोस्ट को पसंद किया है।",
    'ossn:notifications:like:annotation' => "%s ने आपकी टिप्पणी को पसंद किया है।",
    'ossn:notifications:like:entity:file:ossn:aphoto' => "%s ने आपकी तस्वीर पसंद की है।",
    'ossn:notifications:comments:entity:file:ossn:aphoto' => '%s ने आपकी तस्वीर पर टिप्पणी की।',
    'ossn:notifications:wall:friends:tag' => '%s ने आपको एक पोस्ट में टैग किया है।',
    'ossn:notification:are:friends' => 'अब आप दोस्त हैं!',
    'ossn:notifications:comments:post:group:wall' => "%s ने समूह पोस्ट पर टिप्पणी की।",
    'ossn:notifications:like:entity:file:profile:photo' => "%s ने प्रोफ़ाइल फ़ोटो को पसंद किया है।",
    'ossn:notifications:comments:entity:file:profile:photo' => "%s ने प्रोफ़ाइल फ़ोटो पर टिप्पणी की।",
    'ossn:notifications:like:post:group:wall' => '%s ने आपकी पोस्ट पसंद की है।',

    'ossn:notification:delete:friend' => 'मित्र अनुरोध नष्ट कर दिया गया!',
    'notifications' => 'सूचनाएं',
    'see:all' => 'सभी देखें',
    'friend:requests' => 'मित्र अनुरोध',
    'ossn:notifications:friendrequest:confirmbutton' => 'कन्फर्म',
    'ossn:notifications:friendrequest:denybutton' => 'इंकार',

    'ossn:notification:mark:read:success' => 'सफलतापूर्वक सभी को पढ़ने के रूप में चिह्नित',
    'ossn:notification:mark:read:error' => 'सभी को पढ़ने के रूप में चिह्नित नहीं कर सकते',

    'ossn:notifications:mark:as:read' => 'सभी को पढ़िए चिह्नित करें',
);
ossn_register_languages('hi', $en);
