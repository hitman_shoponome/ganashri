<?php
/**
 * Open Source Social Network
 *
 * @packageOpen Source Social Network
 * @author    Open Social Website Core Team <info@informatikon.com>
 * @copyright 2014-2016 SOFTLAB24 LIMITED
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */
$en = array(
    'groups' => 'समूह',
    'add:group' => 'समूह जोड़ें',
    'requests' => 'अनुरोध',

    'members' => 'सदस्य',
    'member:add:error' => 'कुछ गलत हो गया! बाद में पुन: प्रयास करें।',
    'member:added' => 'सदस्यता अनुरोध अनुमोदित!',

    'member:request:deleted' => 'सदस्यता अनुरोध अस्वीकृत हो गया!',
    'member:request:delete:fail' => 'सदस्यता अनुरोध को अस्वीकार नहीं किया जा सकता! बाद में पुन: प्रयास करें।',
    'membership:cancel:succes' => 'सदस्यता अनुरोध रद्द कर दिया!',
    'membership:cancel:fail' => 'सदस्यता अनुरोध को रद्द नहीं किया जा सकता! बाद में पुन: प्रयास करें।',

	'bookkeeping' => 'बहीखाता',
	'bookkeeping:mark' => 'उपस्थित',

    'group:added' => 'समूह सफलतापूर्वक बनाया!',
    'group:add:fail' => 'समूह नहीं बना सकता! बाद में पुन: प्रयास करें।',

    'memebership:sent' => 'अनुरोध सफलतापूर्वक भेजा गया!',
    'memebership:sent:fail' => 'अनुरोध भेज नहीं सकते! बाद में पुन: प्रयास करें।',

    'group:updated' => 'समूह अपडेट कर दिया गया है!',
    'group:update:fail' => 'समूह अपडेट नहीं किया जा सकता! बाद में पुन: प्रयास करें।',

    'group:name' => 'समूह नाम',
    'group:desc' => 'समूह विवरण',
    'privacy:group:public' => 'हर कोई इस समूह और उसके पदों को देख सकता है। केवल सदस्य इस समूह में पोस्ट कर सकते हैं।',
    'privacy:group:close' => 'हर कोई इस समूह को देख सकता है। केवल सदस्य पद पोस्ट और देख सकते हैं।',

    'group:memb:remove' => 'निकालें',
    'group:memb:present' => 'उपस्थित',
    'leave:group' => 'समूह छोड़ें',
    'join:group' => 'समूह में शामिल हों',
    'total:members' => 'कुल सदस्य',
    'group:members' => "सदस्य (%s)",
    'view:all' => 'सभी देखें',
    'member:requests' => 'अनुरोध (%s)',
    'about:group' => 'समूह के बारे में',
    'cancel:membership' => 'सदस्यता रद्द करें',

    'no:requests' => 'कोई अनुरोध नहीं',
    'approve' => 'स्वीकृत करें',
    'decline' => 'अस्वीकार करें',
    'search:groups' => 'समूह खोज',

    'close:group:notice' => 'पोस्ट, फोटो और टिप्पणियों को देखने के लिए इस समूह में शामिल हों।',
    'closed:group' => 'बंद समूह',
    'group:admin' => 'व्यवस्थापक',

	'title:access:private:group' => 'समूह पोस्ट',

	// #186 group join request message var1 = user, var2 = name of group
	'ossn:notifications:group:joinrequest' => '%s ने %s में शामिल होने का अनुरोध किया है',
	'ossn:group:by' => 'द्वारा:',

	'group:deleted' => 'समूह और समूह की सामग्री नष्ट हो गई',
	'group:delete:fail' => 'समूह हटाया नहीं जा सका',
);
ossn_register_languages('hi', $en);
