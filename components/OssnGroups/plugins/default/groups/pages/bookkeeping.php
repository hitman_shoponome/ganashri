<?php
/**
 * Open Source Social Network
 *
 * @packageOpen Source Social Network
 * @author    Open Social Website Core Team <info@informatikon.com>
 * @copyright 2014-2016 SOFTLAB24 LIMITED
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */
$members = $params['group']->getMembers();
$books = $params['group']->getBooks();
$count = $params['group']->getMembers(true);

if($members && (ossn_isAdminLoggedin() || ossn_loggedin_user()->guid == $params['group']->owner_guid)) {
	?>
	<div>
	<h4>People Present Today</h4>
	<form method="post">
	<?php
		foreach ($members as $user) {
		  ?>
			<div class="row">
		        <div class="ossn-group-members">
		        	<div class="col-md-2 col-sm-2 hidden-xs">
			        		<img src="<?php echo $user->iconURL()->large; ?>" width="100" height="100"/>
					</div>
		            <div class="col-md-10 col-sm-10 col-xs-12">
		    	        <div class="uinfo">
		                    <?php
								echo ossn_plugin_view('output/url', array(
										'text' => $user->fullname,
										'href' =>  $user->profileURL(),
										'class' => 'userlink',
								));						
							?>
		    	   		</div>
		                <div class="right request-controls">
		                	<input type="checkbox" name="book_member[]" value=<?php echo "$user->guid"?>>    
		               </div> 
		           </div>
		        </div>           
		    </div>
		<?php
		}
	?>
		<input type="submit" name="<?php echo ossn_print('group:member:present')?>" class="right btn btn-primary">
	</form>
	</div>
	<?php
		if(isset($_REQUEST)){
			$book_member = $_REQUEST['book_member'];
			ossn_add_book($params['group']->owner_guid, $book_member);
		}
	echo ossn_view_pagination($count);
}?>
<!--?php
	$params = array(
		'action' => ossn_site_url() . 'action/group/book',
		'component' => 'OssnGroups',
		'class' => 'ossn-form'
	);
	$form   = ossn_view_form('book', $params, false);
	echo ossn_plugin_view('widget/view', array(
			'title' => ossn_print('book-present'),
			'contents' => $form,
	));
?-->

<?php
if ($books) {
    ?>
	<div>
	<?php
	foreach ($books as $book) {
		$present_members = $book['members'];
		if($present_members) {
			?>
			<div>
			<h4><?php echo gmdate('d F Y H:i:s', $book['time'])?></h4>
			<?php
    		foreach ($present_members as $user) {
    		  ?>
				<div class="row">
			        <div class="ossn-group-members">
    		        	<div class="col-md-2 col-sm-2 hidden-xs">
    			        		<img src="<?php echo $user->iconURL()->large; ?>" width="100" height="100"/>
						</div>
    		            <div class="col-md-10 col-sm-10 col-xs-12">
			    	        <div class="uinfo">
    		                    <?php
									echo ossn_plugin_view('output/url', array(
											'text' => $user->fullname,
											'href' =>  $user->profileURL(),
											'class' => 'userlink',
									));						
								?>
    		    	   		</div>
    		           	</div>
    		        </div>           
    		    </div>
    		<?php
    		} ?>
			<br>
			</div>
			<?php
		}
	}
	?>
	</div> 
	<?php
}?>
