<?php
/**
 * Open Source Social Network
 *
 * @packageOpen Source Social Network
 * @author    Open Social Website Core Team <info@informatikon.com>
 * @copyright 2014-2016 SOFTLAB24 LIMITED
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */
$en = array(
	'com:ossn:invite' => 'आमंत्रित करें',
	'com:ossn:invite:friends' => 'मित्रों को आमंत्रित करें',
	'com:ossn:invite:friends:note' => 'मित्रों को इस नेटवर्क पर शामिल होने के लिए आमंत्रित करने के लिए, उनके ईमेल पते और संक्षिप्त संदेश दर्ज करें। उन्हें आपके निमंत्रण वाला एक ईमेल प्राप्त होगा।',
	'com:ossn:invite:emails:note' => 'ईमेल पतों (अल्पविराम से अलग)',
	'com:ossn:invite:emails:placeholder' => 'smith@example.com, john@example.com',
	'com:ossn:invite:message' => 'संदेश',

    	'com:ossn:invite:mail:subject' => '% s में शामिल होने के लिए निमंत्रण',
    	'com:ossn:invite:mail:message' => 'आपको %s में %s से जुड़ने के लिए आमंत्रित किया गया है उन्होंने निम्नलिखित संदेश शामिल किया था:

%s

शामिल होने के लिए, निम्न लिंक पर क्लिक करें:

%s

प्रोफ़ाइल लिंक: %s
',
	'com:ossn:invite:mail:message:default' => 'नमस्ते,

	मैं आपको %s पर यहां अपने नेटवर्क में शामिल होने के लिए आमंत्रित करना चाहता था।

	प्रोफ़ाइल लिंक: %s

	सादर।
	%s',
	'com:ossn:invite:sent' => 'आपके दोस्तों को आमंत्रित किया गया था। आमंत्रण भेजे गए: %s।',
	'com:ossn:invite:wrong:emails' => 'निम्नलिखित पते मान्य नहीं हैं: %s।',
	'com:ossn:invite:sent:failed' => 'निम्नलिखित पते को आमंत्रित नहीं किया जा सकता: %s।',
	'com:ossn:invite:already:members' => 'निम्नलिखित पते पहले से ही सदस्य हैं: %s',
	'com:ossn:invite:empty:emails' => 'कृपया कम से कम एक ईमेल पता जोड़ें',
);
ossn_register_languages('hi', $en);
