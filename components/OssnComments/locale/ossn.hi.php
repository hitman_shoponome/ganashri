<?php
/**
 * Open Source Social Network
 *
 * @packageOpen Source Social Network
 * @author    Open Social Website Core Team <info@informatikon.com>
 * @copyright 2014-2016 SOFTLAB24 LIMITED
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */
$en = array(
	'write:comment' => 'एक टिप्पणी लिखें ...',
	'like' => 'पसंद',
	'unlike' => 'विपरीत',
	'comment:deleted' => 'टिप्पणी सफलतापूर्वक हटा दी गई!',
	'comment:delete:error' => 'टिप्पणी को हटा नहीं सकते! बाद में पुन: प्रयास करें।',
	'comment:delete' => 'हटाएं',
	'comment:comment' => 'टिप्पणी',
	'comment:view:all' => 'सभी टिप्पणियां देखें',
	'comment:edit:success' => 'टिप्पणी सफलतापूर्वक संपादित की गई',
	'comment:edit:failed' => 'आपकी टिप्पणी को संपादित नहीं किया जा सकता',
);
ossn_register_languages('hi', $en);
