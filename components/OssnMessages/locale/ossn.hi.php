<?php
/**
 * Open Source Social Network
 *
 * @package   (softlab24.com).ossn
 * @author    OSSN Core Team <info@softlab24.com>
 * @copyright 2014-2016 SOFTLAB24 LIMITED
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */
$en = array(
    'user:messages' => 'संदेश',
    'inbox' => 'इनबॉक्स',
    'send' => 'भेजें',
    'ossn:message:between' => 'संदेश %s',
    'messages' => 'संदेश',
    'message:placeholder' => 'यहां पाठ करें',
    'no:messages' => 'आपके पास कोई संदेश नहीं है।'
);
ossn_register_languages('hi', $en);
