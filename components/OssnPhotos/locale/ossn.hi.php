<?php
/**
 * Open Source Social Network
 *
 * @package   (softlab24.com).ossn
 * @author    OSSN Core Team <info@softlab24.com>
 * @copyright 2014-2016 SOFTLAB24 LIMITED
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */

$en = array(
    'album:name' => 'एल्बम नाम',
    'add:album' => 'अल्बम जोड़ें',
    'photo:select' => 'फोटो चुनें',
    'no:albums' => 'कोई एल्बम नहीं',
    'no:photos' => 'कोई तस्वीरें नहीं',
    'back:to:album' => 'एल्बम पर वापस',
    'photo:albums' => 'फोटो एलबम',

    'photo:deleted:success' => 'फोटो सफलतापूर्वक हटाया गया!',
    'photo:delete:error' => 'फ़ोटो हटा नहीं सकते! बाद में पुन: प्रयास करें।',

    'photos' => 'फ़ोटो',
    'back' => 'पीछे',
    'add:photos' => 'फ़ोटो जोड़ें',
    'delete:photo' => 'फ़ोटो हटाएं',

    'covers' => 'कवर',
    'cover:view' => 'कवर व्यू',
    'profile:covers' => 'प्रोफ़ाइल कवर',
	'delete:album' => 'एल्बम हटाएं',

	'photo:album:deleted' => 'फोटो एल्बम सफलतापूर्वक हटाया गया',
	'photo:album:delete:error' => 'फोटो एल्बम को हटाया नहीं जा सकता',


);
ossn_register_languages('hi', $en);
