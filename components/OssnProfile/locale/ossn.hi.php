<?php
/**
 * Open Source Social Network
 *
 * @packageOpen Source Social Network
 * @author    Open Social Website Core Team <info@informatikon.com>
 * @copyright 2014-2016 SOFTLAB24 LIMITED
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */
$en = array(
		'people:like:this' => 'लोग जो इस पोस्ट को पसंद करते हैं',
		'change:cover' => 'कवर बदलें',
		'change:photo' => 'फोटो बदलें',
		'update:info' => 'अद्यतन जानकारी',
		'message' => 'संदेश',
		'save:position' => 'स्थिति सहेजें',
		'ossn:profile:picture:updated' => 'प्रोफ़ाइल चित्र बदल दिया गया।',
		'ossn:profile:cover:picture:updated' => 'प्रोफ़ाइल कवर बदल दिया गया।',
		'language' => 'भाषा',

		'edit:profile' => 'प्रोफ़ाइल संपादित करें',
		'reposition:cover' => 'पुनर्स्थापन',
		'profile:photos' => 'प्रोफाइल फ़ोटो',

		'profile:cover:err1' => 'कवर छवि बहुत छोटी है',
		'profile:cover:err1:detail' => 'कवर चित्र कम से कम 850 x 300 या अधिक होना चाहिए।'
);
ossn_register_languages('hi', $en);
