<?php
/**
 * Open Source Social Network
 *
 * @package   (softlab24.com).ossn
 * @author    OSSN Core Team <info@softlab24.com>
 * @copyright 2014-2016 SOFTLAB24 LIMITED
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */
$error = input('error');
?>
<div class="row">
	<div class="col-md-6 ossn-page-contents">
	<?php if ($error == 1) { ?>
		<div class="alert alert-danger">
			<strong><?php echo ossn_print('login:error'); ?></strong><br/>
			<p><?php echo ossn_print('login:error:sub'); ?></p>
		</div>
	<?php } ?>
	<?php
		$contents = ossn_view_form('login-provider', array(
			'id' => 'ossn-home-login',
			'action' => ossn_site_url('action/provider/login'),
		));

		echo ossn_plugin_view('widget/view', array(
			'title' => ossn_print('site:providerlogin'),
			'contents' => $contents,
		));
	?>
	</div>
	<div class="col-md-6 ossn-page-contents">
	<?php
		$contents = ossn_view_form('signup', array(
			'id' => 'ossn-home-signup',
			'action' => ossn_site_url('action/provider/register')
		));

		echo ossn_plugin_view('widget/view', array(
			'title' => ossn_print('create:account'),
			'contents' => $heading.$contents,
		));
	?>
	</div>
</div>
