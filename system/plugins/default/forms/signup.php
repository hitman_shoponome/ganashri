<?php
/**
 * Open Source Social Network
 *
 * @packageOpen Source Social Network
 * @author    Open Social Website Core Team <info@informatikon.com>
 * @copyright 2014-2016 SOFTLAB24 LIMITED
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */
?>
<div>
    <label><?php echo ossn_print('type:organisation'); ?></label>
    <select class="selectpicker" name="orgtype">
        <option value='ngo'><?php echo ossn_print('ngo'); ?></option>
        <option value='government:organisation'><?php echo ossn_print('government:organisation'); ?></option>
        <option value='business'><?php echo ossn_print('business'); ?></option>
        <option value="bank"><?php echo ossn_print('bank'); ?></option>
    </select>
</div>
<div>
    <input type="text" name="nameorg" placeholder="<?php echo ossn_print('name:org'); ?>"/>
    <input type="text" name="purposeorg" placeholder="<?php echo ossn_print('purpose:org'); ?>"/>
</div>

<div>
    <input type="text" name="username" placeholder="<?php echo ossn_print('username:org'); ?>"/>
    <input type="password" name="password" placeholder="<?php echo ossn_print('password:org'); ?>"/>
</div>

<div>
    <input type="text" name="website" placeholder="<?php echo ossn_print('website'); ?>"/>
</div>

<div>
    <label><?php echo ossn_print('contact:details'); ?></label>
    <input type="text" name="contactname" placeholder="<?php echo ossn_print('contact:name'); ?>" class="long-input"/>
</div>

<div>
    <input type="text" name="email" placeholder="<?php echo ossn_print('email'); ?>" class="long-input"/>
    <input type="text" name="contactnumber" placeholder="<?php echo ossn_print('contact:number'); ?>" class="long-input"/>
    <input type="text" name="corraddress" placeholder="<?php echo ossn_print('corr:address'); ?>" class="long-input"/>
</div>

<div>
<?php echo ossn_fetch_extend_views('forms/signup/before/submit'); ?>
</div>

<div id="ossn-signup-errors" class="alert alert-danger"></div>

<div class="ossn-loading ossn-hidden"></div>
<input type="submit" id="ossn-submit-button" class="btn btn-success" value="<?php echo ossn_print('create:account'); ?>" class=""/>
