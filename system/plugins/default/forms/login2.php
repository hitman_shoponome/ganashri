<?php
/**
 * Open Source Social Network
 *
 * @packageOpen Source Social Network
 * @author    Open Social Website Core Team <info@informatikon.com>
 * @copyright 2014-2016 SOFTLAB24 LIMITED
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */
?>
<div>
	<label><?php echo ossn_print('bhamashah_id'); ?></label>
    <input type="text" name="bhamashah_id" />
    <input type="" value="<?php echo ossn_print('site:otp');?>" class="btn btn-primary"/>
</div>

<div>
	<label><?php echo ossn_print('otp'); ?></label>
    <input type="password" name="password" />
</div>
<div>
	<?php echo ossn_fetch_extend_views('forms/login2/before/submit'); ?>
<div>
<div>
    <input type="submit" value="<?php echo ossn_print('site:login');?>" class="btn btn-primary"/>
</div>
