<?php
/**
 * Open Source Social Network
 *
 * @packageOpen Source Social Network
 * @author    Informatikon Core Team <info@informatikon.com>
 * @copyright 2014-2016 SOFTLAB24 LIMITED
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      http://www.informatikon.com/
 */
session_start();
//error_log("In start.php \r\n", 3, "/tmp/ossn_error.log");
global $Ossn;
if (!isset($Ossn)) {
    $Ossn = new stdClass;
}

include_once(dirname(dirname(__FILE__)) . '/libraries/ossn.lib.route.php');

if (!is_file(ossn_route()->configs . 'ossn.config.site.php') && !is_file(ossn_route()->configs . 'ossn.config.db.php')) {
    header("Location: installation");
	exit;
}
include_once(ossn_route()->configs . 'libraries.php');
include_once(ossn_route()->configs . 'classes.php');

include_once(ossn_route()->configs . 'ossn.config.site.php');
include_once(ossn_route()->configs . 'ossn.config.db.php');

foreach ($Ossn->classes as $class) {
    //error_log("In classes ".$class." \r\n", 3, "/tmp/ossn_error.log");
    if (!include_once(ossn_route()->classes . "Ossn{$class}.php")) {
        throw new exception('Cannot include all classes');
    }
}
//error_log("End of classes \r\n", 3, "/tmp/ossn_error.log");
foreach ($Ossn->libraries as $lib) {
    //error_log("In libraries including ".$lib."\r\n", 3, "/tmp/ossn_error.log");
    if (!include_once(ossn_route()->libs . "ossn.lib.{$lib}.php")) {
        throw new exception('Cannot include all libraries');
    }
}
//error_log("End of libraries \r\n", 3, "/tmp/ossn_error.log");
ossn_trigger_callback('ossn', 'init');
//need to update user last_action
// @note why its here?
//error_log("back from callback \r\n", 3, "/tmp/ossn_error.log");
update_last_activity();
//error_log("back from update_last_activity \r\n", 3, "/tmp/ossn_error.log");
//update_provider_last_activity();
