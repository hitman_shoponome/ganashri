========
GANASHRI
========
GanaShri is a unique social networking platform targeting the needs of rural
women. For a detailed description of the idea, value proposition and marketing
strategy please refer to the idea document. For a quick introduction to GanaShri
please watch following 2 minute video :
    https://youtu.be/Xuxl1o_IKRY
    [Note: For high-quality video formats please open the video link in one of
    the following browser and operating system combinations:
        - Google Chrome (all operating systems)
        - Internet Explorer or Edge on Windows 8.1 or newer
        - Safari on Mac OS X 10.10 or newer
        - Firefox on Windows 7 or newer and on Mac OS X 10.10 or newer
    ]

=========
CONTENTS
=========
This document contains:
    - Instructions to operate live demo of GanaShri
    - Features of GanaShri
    - Technologies used in making GanaShri
    - Requirements for running GanaShri on your server/localhost
    - Link to Repository URL
    - Instructions to install GanaShri on a personal server
    - Known Bugs
    - Contact information of programmers

=============================================
Instructions to operate live demo of GanaShri
=============================================
We have tried to keep the interface as intuitive as possible. GanaShri's
user interface is similar to most of the social networking platforms. Please
visit the following url to operate the live demo.
    http://54.70.91.106/
And use any of the 7 digit Bhamashah Family Id along with 1234 OTP for Login.

====================
Features of GanaShri
====================
We have implemented some crucial features catering to the needs of rural women, Self-help groups and Self-help promoting institutions.
These features are as follows:
        * Automatic Signup using Bhamashah Family Id and OTP on your mobile number registered with Bhamashah
        * Provider Registration
        * Provider Login
        * Self-help Group(SHG) Formation
        * Adding users to SHG
        * Bookkeeping for SHG
        * Interface available in Hindi as well
GanaShri also contains most of the features present in any social-networking platform like:
        * User Login
        * Profile
        * Profile Photo
        * Profile Cover
        * Add/Remove Friends
        * Live Chat
        * Wall posts
        * Photos
        * Ads
        * Tag friends in posts
        * User block system
        * User poke system
        * Ajax Comments
        * Ajax Likes
        * Ajax Photos in comments
        * Group cover photos
        * Reposition Profile/Group cover
        * Notifications
        * Friend Requests
        * Chat Bar
        * Invite Friends
        * Embed Videos
        * Smilies
        * Site-pages (terms, privacy, about)
        * Site Search
        * Reset Password
        * Newsfeed page
        * Post Edit
        * Comment Edit
        * Mobile Friendly

====================================
Technologies used in making GanaShri
====================================
        * Bhamashah API
        * PHP
        * Javascript
        * CSS
        * HTML
        * Bootstrap
        * MySql
        * Apache
        * JSON
        * XML
        * OSSN(OSSN is an opensource software which helped use to complete This daunting task in the limited time period)

==========================================================
Requirements for running GanaShri on your server/localhost
==========================================================
        * PHP 5.4 OR >
        * MYSQL 5 OR >
        * APACHE
        * MOD_REWRITE
        * PHP Function cURL
        * PHP GD Library
        * PHP ZIP Extension
        * PHP settings allowurlfopen enabled
        * JSON Support
        * XML Support
        * In case of your own server:
            - 512MB RAM
            - 10GB Disk , More disk more data can be stored.
            - 1v CPU
            - Should be Ubuntu Server x64 bits.

======================
Link to Repository URL
======================
        https://AnshulYADAV007@bitbucket.org/hitman_shoponome/ganashri.git

===========================================================
Instructions to install GanaShri on a personal server/Linux
===========================================================
STEP 1: Update and upgrade to make your OS up-to-date:
            sudo apt-get update
            sudo apt-get upgrade
STEP 2: Install Apache2
            sudo apt-get install apache2 apache2-doc apache2-utils
STEP 3: Edit the main Apache configuration file and turn off the
        KeepAlive setting:
            File : /etc/apache2/apache2.conf
                KeepAlive Off
STEP 4: Enable Apache rewrite module from terminal. Paste the following command
            sudo a2enmod rewrite
STEP 5: Install PHP
            sudo apt-get install php5 libapache2-mod-php5
STEP 6: Install Mysql
            sudo apt-get install mysql-server
STEP 7: Install required PHP packages
            apt-get install php7.0-cli php7.0-common php7.0-json php7.0-mcrypt php7.0-mysql php7.0-xml php7.0-curl php7.0-zip php7.0-gd
STEP 8: Clone our git directory from bitbucket

STEP 9: Create a new MySQL database and user for GanaShri:
            mysql -u root -p
            mysql> SET GLOBAL sql_mode='';
            mysql> CREATE DATABASE ganashridb;
            mysql> CREATE USER 'ganashriuser'@'localhost' IDENTIFIED BY 'y0ur-pAssW0RD';
            mysql> GRANT ALL PRIVILEGES ON ganashridb.* TO 'ganashriuser'@'localhost';
            mysql> FLUSH PRIVILEGES;
            mysql> quit
STEP 10: Make the required alterations in DB
            mysql -u ganashriuser < ganashri.sql -p'y0ur-pAssW0RD'
STEP 11: Edit the PHP configuration file (/etc/php/7.0/cli/php.ini):
                vim /etc/php/7.0/cli/php.ini
            Modify these lines:
                allow_url_fopen = On
                file_uploads = On
                upload_max_filesize = 32M
STEP 12: All files have to be readable by the web server, so set a proper
        ownership:
                chown www-data:www-data -R /var/www/html/ganashri/
STEP 13: Create a new virtual host directive in Apache

            touch /etc/apache2/sites-available/ganashri.conf

            ln -s /etc/apache2/sites-available/ganashri.conf /etc/apache2/sites-enabled/ganashri.conf

            vim /etc/apache2/sites-available/ganashri.conf

        Add the following lines:
            <VirtualHost *:80>
            ServerAdmin admin@your-domain.com
            DocumentRoot /var/www/html/ganashri/
            ServerName your-domain.com
            ServerAlias www.your-domain.com
            <Directory /var/www/html/ganashri/>
            Options FollowSymLinks
            AllowOverride All
            Order allow,deny
            allow from all
            </Directory>
            ErrorLog /var/log/apache2/your-domain.com-error_log
            CustomLog /var/log/apache2/your-domain.com-access_log common
            </VirtualHost>
        Remove the 000-default.conf file:
            rm /etc/apache2/sites-enabled/000-default.conf
STEP 14: Restart the Apache service for the changes to take effect
            service apache2 restart
STEP 15: Visit your site
        Once you’ve performed these steps, visit your site in your web browser.
        GanaShri will take you through the rest of the installation process from there. The first account that you create at the end of the installation process will be an administrator account.
STEP 16: Execute the following commands to change the language to Hindi
            mysql -u ganashriuser -p'y0ur-pAssW0RD'
mysql> update ganashridb.ossn_site_settings set value = 'hi' where setting_id = 3;

==============================
Known Bugs (We are human too.)
==============================

==================================
Contact information of programmers
==================================
ANSHUL YADAV
      * Mobile number : 8185970934
      * Email id : anshulyadav98@gmail.com
Sonil Yadav
      * Mobile number : 7416848139
      * Email id : sonil.yadav.182@gmail.com
