<?php
/**
 * Open Source Social Network
 *
 * @package   (softlab24.com).ossn
 * @author    OSSN Core Team <info@softlab24.com>
 * @copyright 2014-2016 SOFTLAB24 LIMITED
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */

$hi = array(
	'home' => 'घर',
	'site:index' => 'स्वागत',
	'news:feed' => 'समाचार फ़ीड',
	'new:feed' => 'समाचार फ़ीड',
	'photos:ossn' => 'फ़ोटो',
	'admin:view:site' => 'साइट देखें',

	'home:top:heading' => "%s में आपका स्वागत है। अपने भामाशाह परिवार के आईडी का उपयोग करें, नए दोस्त बनाने के लिए, स्वयं सहायता समूह बनाएं, नई स्कील सीखें, और बहुत कुछ",
	'create:account' => 'एक खाता बनाएं',
	'its:free' => "यह मुफ़्त है और हमेशा रहेगा।",
	'welcome:ganashri' => "गणेश्री में आपका स्वागत है",

	'register:ok:message' => "आपका खाता पंजीकृत है। एक सक्रियण ईमेल आपके मेलबॉक्स पर भेज दी गई है। अगर आपको ईमेल नहीं मिला है, तो कृपया अपना स्पैम / जंक फ़ोल्डर देखें।",

	'copyright' => '&copy; कॉपीराइट',
  'powered' => 'Powered by the Open Source Social Network.',

	'name' => 'नाम',
	'birthdate' => 'जन्मदिन',
	'first:name' => 'प्रथम नाम',
	'last:name' => 'अंतिम नाम',
	'email' => 'ईमेल',
	'email:again' => 'ईमेल फिर से दर्ज करें',
	'username' => 'उपयोगकर्ता नाम',
	'bhamashah_id' => 'भामाशा परिवार आईडी',
	'password' => 'पासवर्ड',
	'day' => 'दिन',
	'month' => 'महीना',
	'year' => 'वर्ष',
	'male' => 'पुरुष',
	'female' => 'महिला',
	'account:create:notice' => 'एक खाता बनाएं क्लिक करके आप हमारे से सहमत हैं',
	'gender' => 'लिंग',

	'type:organisation' => 'Type of Organisation',
	'ngo' => 'NGO',
	'government:organisation' => 'Government Organisation',
	'business' => 'Buisness',
	'bank' => 'Bank',
	'name:org' => 'Name of the Organisation',
	'purpose:org' => 'Purpose of registration',
	'password:org' => 'Password',
	'username:org' => 'Username for your account',
	'website' => 'Website',
	'contact:details' => 'Contact Details',
	'contact:name' => 'Contact Person\'s Name',
	'contact:number' => 'Contact Number',
	'corr:address' => 'Correspondence Address',

	'account:settings' => 'खाता सेटिंग्स',
	'page:error' => 'पृष्ठ नहीं मिला!',
	'page:error:text' => 'आपके द्वारा अनुरोधित पृष्ठ अनुपलब्ध है या हटा दिया गया हो सकता है।',
	'page:error:heading' => '404',

	'search:users' => 'लोग',
	'admin:logout' => 'लॉग आउट',
	'logout' => 'लॉग आउट',

	'ossn:like:this' => '%s ने इसे पसंद किया',
	'ossn:like:you:and:this' => 'आप और %s ने इसे पसंद किया',
	'ossn:like:people' => '%s लोग',
	'ossn:like:person' => '%s व्यक्ति',
	'ossn:liked:you' => 'आपको यह पसंद आया',
	'ossn:unlike' => 'विपरीत',
	'ossn:like' => 'पसंद',

	'admin:components' => 'अवयव',
	'admin:dashboard' => 'डैशबोर्ड',
	'admin:install' => 'इंस्टॉलर',
	'admin:themes' => 'थीम्स',
	'admin:basic' => 'बेसिक',
	'admin:cache' => 'कैश',
	'admin:mode' => 'मोड',
	'configure' => 'कॉन्फ़िगर',
	'admin:help' => 'सहायता',
	'admin:support' => 'समर्थन',

	'ossn:update:check:error' => 'त्रुटि',

	'upload' => 'अपलोड करें',
	'save' => 'सहेजें',
	'add' => 'जोड़ें',
	'edit' => 'संपादित करें',
	'search' => 'खोज',
	'delete' => 'हटाएं',

	'admin:users' => 'सूची उपयोगकर्ता',
	'admin:add:user' => 'उपयोगकर्ता जोड़ें',
	'admin:user:deleted' => 'उपयोगकर्ता हटा दिया गया है!',
	'admin:user:delete:error' => 'उपयोगकर्ता को हटाने में असमर्थ बाद में पुन: प्रयास करें।',

	'type' => 'प्रकार',
	'normal' => 'सामान्य',
	'admin' => 'प्रशासक',

	'lastlogin' => 'अंतिम लॉगिन',

	'my:version' => 'मेरा ओएसएसएन संस्करण',
	'online:users' => 'उपयोगकर्ता ऑनलाइन',
	'themes' => 'थीम्स',
	'users' => 'उपयोगकर्ता',
	'components' => 'अवयव',
	'available:updates' => 'उपलब्ध अपडेट',
	'website:name' => 'वेबसाइट नाम',
	'owner:email' => 'स्वामी ईमेल',
	'default:lang' => 'डिफ़ॉल्ट भाषा',

	'timeline' => 'टाइमलाइन',
	'photos' => 'फ़ोटो',
	'friends' => 'मित्र',
	'links' => 'लिंक',
	'ossn:add:user:mail:subject' => "%s कृपया अपना ईमेल पता %s के लिए पुष्टि करें!",
	'ossn:add:user:mail:body' => "इससे पहले कि आप% s का प्रयोग शुरू कर सकें, आपको अपने ईमेल पते की पुष्टि करनी होगी।

कृपया नीचे दिए गए लिंक पर क्लिक करके अपने ईमेल पते की पुष्टि करें:

%s

आप लिंक को कॉपी और अपने ब्राउजर पर मैन्युअल रूप से पेस्ट कर सकते हैं यदि लिंक काम नहीं कर रहा है।

%s",
	'user:friends' => 'मित्र',
	'user:account:validated' => 'खाता सफलतापूर्वक मान्य किया गया है!',
	'user:account:validate:fail' => 'खाता मान्य नहीं कर सकता! बाद में पुन: प्रयास करें।',

	'cache:enabled' => 'कैश सफलतापूर्वक सक्षम!',
	'cache:disabled' => 'कैश को सफलतापूर्वक अक्षम किया गया!',
	'cache:enable' => 'कैश सक्षम करें',
	'cache:disable' => 'कैश अक्षम करें',

	'cache:1' => 'सक्षम',
	'cache:0' => 'अक्षम',
	'cache:notice' => 'साइट के प्रदर्शन को बेहतर बनाने के लिए कैश में स्टोर सीएसएस और जावास्क्रिप्ट फाइलें।',

	'theme:install:notice' => 'एक वैध। ज़िप थीम पैकेज अपलोड करें।',
	'com:install:notice' => 'एक वैध। ज़िप घटक पैकेज अपलोड करें।',

	'login:error' => 'अमान्य उपयोगकर्ता नाम या पासशब्द!',
	'login:error:sub' => "हम आपको लॉग इन नहीं कर सके। कृपया अपना यूज़रनेम और पासवर्ड जांचें और पुनः प्रयास करें।",
	'login:success' => 'अब आप लॉग इन हैं!',
	'com:installed' => 'घटक आपके घटक सूची में अपलोड किया गया है, अब आप इसे घटकों पृष्ठ से सक्षम कर सकते हैं।',
	'com:install:error' => 'घटक अपलोड नहीं किया जा सकता, सुनिश्चित करें कि यह मान्य पैकेज है।',
	'settings:saved' => 'सेटिंग्स सहेजी गईं',
	'com:deleted' => 'घटक हटा दिया गया है!',
	'com:delete:error' => 'घटक हटा नहीं सकते! कृपया पुन: प्रयास करें',
	'com:disabled' => 'सफलतापूर्वक अक्षम!',
	'com:enabled' => 'घटक सक्षम!',

	'theme:delete:active' => 'सक्रिय थीम को हटा नहीं सकता।',
	'theme:deleted' => 'थीम हटा दी गई है।',
	'theme:delete:error' => 'थीम हटा नहीं सकते! बाद में पुन: प्रयास करें।',
	'theme:enabled' => 'थीम सफलतापूर्वक सक्रिय हो गई है!',
	'theme:installed' => 'थीम सफलतापूर्वक स्थापित हो गई है!',
	'theme:install:error' => 'थीम स्थापित करने में असमर्थ। सुनिश्चित करें कि यह आपके द्वारा उपयोग किए जाने वाले ओएसएसएन संस्करण के साथ संगत है।',

	'fields:require' => 'सभी फ़ील्ड आवश्यक हैं!',
	'username:error' => 'उपयोगकर्ता नाम अमान्य है।',
	'password:error' => 'पासवर्ड 5 वर्णों से अधिक होना चाहिए।',
	'account:created' => 'आपका खाता बना दिया गया है।',
	'account:create:error:admin' => 'खाता पंजीकरण विफल रहा! बाद में पुन: प्रयास करें।',
	'user:updated' => 'उपयोगकर्ता अपडेट कर दिया गया है!',
	'logged:out' => 'अब आप लॉग आउट हैं!',
	'username:inuse' => 'निम्न उपयोगकर्ता नाम पहले ही उपयोग किया जा चुका है। कृपया एक भिन्न उपयोगकर्ता नाम चुनें।',
	'email:inuse' => 'आपके द्वारा उपयोग किया गया ईमेल पता पहले से ही हमारे डेटाबेस में मौजूद है कृपया एक और ईमेल पते की कोशिश करें।',
	'email:invalid' => 'ईमेल पता अमान्य है! सुनिश्चित करें कि आपने जो ईमेल पता दर्ज किया वह सही है।',
	'email:error:matching' => "ईमेल पते मेल नहीं खाते हैं।",
	'account:created:email' => "आपका खाता पंजीकृत किया गया है! हमने आपको एक खाता सक्रियण ईमेल भेजा है। अगर आपको ईमेल नहीं मिला है, तो कृपया अपना स्पैम / जंक फ़ोल्डर देखें",

	'administration' => 'प्रशासन',

	'privacy' => 'गोपनीयता',
	'close' => 'स्व-सहायता समूह',
	'open' => 'ओपन',
	'public' => 'सार्वजनिक',
	'friends' => 'मित्र',
	'privacy:public:note' => 'इस साइट पर हर कोई यह देख सकता है।',
	'privacy:friends:note' => 'केवल आपके दोस्त ही देख सकते हैं।',

	'add:friend' => 'मित्र जोड़ें',
	'cancel:request' => 'अनुरोध रद्द करें',
	'remove:friend' => 'अनफ्रेन्ड',
	'no:friends' => 'कोई दोस्त नहीं',
	'settings' => 'सेटिंग्स',
	'ossn:add:friend:error' => 'मित्र को जोड़ नहीं सकते!',
	'ossn:friend:request:submitted' => 'आपका मित्र अनुरोध भेजा गया है!',

	/* Upgrades */
	'upgrade:file:load:error' => 'अपग्रेड फ़ाइल लोड नहीं कर पाई!',
	'upgrade:success' => "आपकी वेबसाइट %s को रिलीज़ करने के लिए सफलतापूर्वक उन्नत किया गया है।",
	'upgrade:failed' => "%s को रिलीज करने के लिए आपकी साइट का उन्नयन नहीं किया जा सकता है।",
	'upgrade:not:available' => 'अपग्रेड करने के लिए कुछ नहीं!',

	'site:login' => 'भामाशाह आईडी का उपयोग कर लॉगिन करें',
	'site:otp' => 'लॉग इन ओटीपी प्राप्त करें',
	'otp' => 'कृपया ओटीपी दर्ज करें',
	'site:providerlogin' => 'प्रदाता के रूप में प्रवेश करें',
	/* Page errors */
	'system:error:title' => 'कुछ गलत हो गया!',
	'system:error:text' => 'कुछ गलत हो गया! इस पृष्ठ को पुनः लोड करें और पुनः प्रयास करें।',

	/* Password Reset */
	'reset:login' => 'पासवर्ड रीसेट करें',
	'reset:password' => 'अपना पासवर्ड रीसेट करें',
	'enter:new:password' => 'नीचे अपना नया पासवर्ड दर्ज करें और रीसेट करें पर क्लिक करें।',
	'reset' => 'रीसेट',
	'enter:emai:reset:pwd' => 'पासवर्ड रीसेट करने के लिए अपने खाते का ईमेल पता दर्ज करें।',
	'ossn:reset:password:subject' => 'अपना पासवर्ड रीसेट करें',
	'ossn:reset:password:body' => "हैलो %s,

कृपया पासवर्ड रीसेट करने के लिए नीचे दिए गए लिंक पर क्लिक करें या लिंक कॉपी करें और अपने ब्राउज़र के पता बार में पेस्ट करें:

%s

%s",
	'passord:reset:success' => 'पासवर्ड सफलतापूर्वक बदल गया!',
	'passord:reset:fail' => 'पासवर्ड रीसेट नहीं कर सकता! बाद में पुन: प्रयास करें।',
	'password:reset:email:required' => 'ईमेल रिक्त नहीं हो सकता!',
	'passord:reset:email:success' => 'पासवर्ड रीसेट निर्देश आपके ईमेल पते पर भेज दिए गए हैं।',
	'reset:password' => 'पासवर्ड रीसेट करें',

	'erros:reporting' => 'त्रुटि रिपोर्टिंग',
	'erros:off' => 'ऑफ',
	'erros:on' => 'चालू',
	'basic:settings' => 'बेसिक सेटिंग्स',

	'ossn:new:version:error' => 'अज्ञात',
	'ossn:version:avaialbe' => "%s",

	'ossn:exception:title' => 'सिस्टम त्रुटि हुई है। बाद में पुन: प्रयास करें। आप इस त्रुटि के विवरण %s पर सिस्टम व्यवस्थापक को ईमेल कर सकते हैं।',
	'ossn:securitytoken:failed' => 'आपके द्वारा अनुरोधित क्रिया अमान्य है।',
	'ossn:component:delete:exception' => 'क्या आप वाकई इस घटक को हटाना चाहते हैं?',
	'ossn:user:delete:exception' => 'क्या आप इस उपयोगकर्ता को हटाना चाहते हैं?',

	//access
	'title:access:1' => 'निजी',
	'title:access:2' => 'सार्वजनिक',
	'title:access:3' => 'मित्र',

	'upload:file:error:ini_size' => 'आपके द्वारा अपलोड की जाने वाली फ़ाइल बहुत बड़ी है।',
	'upload:file:error:form_size' => 'आपके द्वारा अपलोड की जाने वाली फ़ाइल बहुत बड़ी है।',
	'upload:file:error:partial' => 'फ़ाइल अपलोड पूर्ण नहीं हुई।',
	'upload:file:error:no_file' => 'कोई फ़ाइल नहीं चुनी गई थी।',
	'upload:file:error:no_tmp_dir' => 'अपलोड की गई फ़ाइल को नहीं बचा सकता।',
	'upload:file:error:cant_write' => 'अपलोड की गई फ़ाइल को नहीं बचा सकता।',
	'upload:file:error:extension' => 'अपलोड की गई फ़ाइल को नहीं बचा सकता।',
	'upload:file:error:unknown' => 'फ़ाइल अपलोड विफल हुई।',

	'ossn:post:size:exceed' => 'आपके द्वारा अनुरोधित क्रिया आकार की सीमा से अधिक है।',

	'admin:components' => 'अवयव',
	'admin:themes' => 'थीम्स',
	'admin:com:installer' => 'घटक इंस्टॉलर',
	'admin:theme:installer' => 'थीम इंस्टॉलर',
	'admin:cache:settings' => 'कैश सेटिंग्स',
	'admin:add:user' => 'उपयोगकर्ता जोड़ें',
	'admin:user:list' => 'उपयोगकर्ता सूची',
	'admin:edit:user' => 'उपयोगकर्ता संपादित करें',
	'admin:login' => 'लॉगिन',
	'admin:notification:email' => 'साइट अधिसूचना ईमेल',
	'notification_email' => 'अधिसूचना ईमेल (noreply@domain.com)',
	'owner_email' => 'स्वामी ईमेल (mysite@domain.com)',
	'ossn:websitename' => 'आपकी वेबसाइट का नाम',
	'ossn:user:validation:resend' => 'आपका खाता मान्य नहीं है! प्रवेश करने से पहले आपको अपना अकाउंट सत्यापित करना होगा। एक और सत्यापन ईमेल आपके ईमेल पते पर भेज दिया गया है।',
	'site:timepassed:text' => '%s पहले',
	// localization of passed time: 16 time elements (even = singular / odd = plural)
	'site:timepassed:data' => "सेकंड | सेकंड | मिनट | मिनट | घंटे | घंटे | दिन | दिन | सप्ताह | सप्ताह | महीने | महीने | साल | साल | दशक | दशक",
	'ossn:notification:no:notification' => 'दिखाने के लिए कुछ नहीं',
	'admin:button:enable' => 'सक्षम करें',
	'admin:button:enabled' => 'सक्षम',
	'admin:button:disable' => 'अक्षम',
	'admin:button:configure' => 'कॉन्फ़िगर',
	'admin:button:delete' => 'हटाएं',
	'admin:component:author' => 'लेखक',
	'admin:component:website' => 'वेबसाइट',
	'admin:sidemenu:components' => 'अवयव',
	'admin:sidemenu:themes' => 'थीम्स',
	'admin:sidemenu:settings' => 'साइट सेटिंग्स',
	'admin:sidemenu:usermanager' => 'उपयोगकर्ता प्रबंधक',
	'cancel' => 'रद्द करें',
	'ossn:language:complete' => 'पूर्ण',
	'ossn:pagination:first' => 'पहले',
	'ossn:pagination:last' => 'अंतिम',
	'validate' => 'मान्य',
	'admin:users:unvalidated' => 'अमान्यकृत उपयोगकर्ता',
	'admin:user:validated' => 'उपयोगकर्ता मान्य',
	'admin:user:validate:error' => 'उपयोगकर्ता को मान्य नहीं किया जा सकता।',

	//v3.0
	//admin
	'my:files:version' => 'मेरे ओएसएन फाइल संस्करण',
	'cache:flush:error' => 'कैश फ़्लश नहीं कर सकता, यह सुनिश्चित करें कैश सेटिंग्स कैश में सक्षम है',
	'cache:flushed' => 'कैश सफलतापूर्वक फ्लश',
	'ossn:version' => 'ओएसएसएन संस्करण',
	'php:extension' => 'PHP एक्सटेंशन',
	'php:version' => 'PHP संस्करण',
	'php:function' => 'PHP फ़ंक्शन',
	'admin:old:com' => "आपका ossn_com.xml file फाइल ओएसएन के पुराने संस्करण पर आधारित है। कृपया '%s' घटक को अपडेट करें।",
	'admin:old:theme' => "आपका ossn_theme.xml फाइल ओएसएन के पुराने संस्करण पर आधारित है। कृपया '%s' थीम को अपडेट करें।",
	'admin:button:disabled' => 'अक्षम',
	'admin:flush:cache' => 'फ्लश कैश',
	'admin:com:version' => 'संस्करण',
	'admin:com:author' => 'लेखक',
	'admin:com:author:url' => 'लेखक यूआरएल',
	'admin:com:license' => 'लाइसेंस',
	'admin:com:requirements' => 'आवश्यकताएं',
	'admin:com:availability' => 'उपलब्धता',
	'ossn:exception:make:sure' => 'क्या आप निश्चित हैं?',
	'ossn:premium' => 'प्रीमियम संस्करण',
	'datepicker:days' => "रविवार, सोमवार, मंगलवार, बुधवार, गुरूवार, शुक्रवार, शनिवार",
	'datepicker:months' => "जनवरी, फरवरी, मार्च, अप्रैल, मई, जून, जुलाई, अगस्त, सितंबर, अक्टूबर, नवंबर, दिसम्बर",

  /*
	 * List of ISO 639-1 language codes
	 * http://en.wikipedia.org/wiki/List_of_ISO_639-1_codes
	 */
	"aa" => "Afar",
	"ab" => "Abkhazian",
	 "af" => "Afrikaans",
	"am" => "Amharic",
	"ar" => "Arabic",
	"as" => "Assamese",
	"ay" => "Aymara",
	"az" => "Azerbaijani",
	"ba" => "Bashkir",
	"be" => "Byelorussian",
	"bg" => "Bulgarian",
	"bh" => "Bihari",
	"bi" => "Bislama",
	"bn" => "Bengali; Bangla",
	"bo" => "Tibetan",
	"br" => "Breton",
	"ca" => "Catalan",
	"co" => "Corsican",
	"cs" => "Czech",
	"cy" => "Welsh",
	"da" => "Danish",
	"de" => "German",
	"dz" => "Bhutani",
	"el" => "Greek",
	"en" => "English",
	"eo" => "Esperanto",
	"es" => "Spanish",
	"et" => "Estonian",
	"eu" => "Basque",
	"fa" => "Persian",
	"fi" => "Finnish",
	"fj" => "Fiji",
	"fo" => "Faeroese",
	"fr" => "French",
	"fy" => "Frisian",
	"ga" => "Irish",
	"gd" => "Scots / Gaelic",
	"gl" => "Galician",
	"gn" => "Guarani",
	"gu" => "Gujarati",
	"he" => "Hebrew",
	"ha" => "Hausa",
	"hi" => "Hindi",
	"hr" => "Croatian",
	"hu" => "Hungarian",
	"hy" => "Armenian",
	"ia" => "Interlingua",
	"id" => "Indonesian",
	"ie" => "Interlingue",
	"ik" => "Inupiak",
	"is" => "Icelandic",
	"it" => "Italian",
	"iu" => "Inuktitut",
	"iw" => "Hebrew (obsolete)",
	"ja" => "Japanese",
	"ji" => "Yiddish (obsolete)",
	"jw" => "Javanese",
	"ka" => "Georgian",
	"kk" => "Kazakh",
	"kl" => "Greenlandic",
	"km" => "Cambodian",
	"kn" => "Kannada",
	"ko" => "Korean",
	"ks" => "Kashmiri",
	"ku" => "Kurdish",
	"ky" => "Kirghiz",
	"la" => "Latin",
	"ln" => "Lingala",
	"lo" => "Laothian",
	"lt" => "Lithuanian",
	"lv" => "Latvian/Lettish",
	"mg" => "Malagasy",
	"mi" => "Maori",
	"mk" => "Macedonian",
	"ml" => "Malayalam",
	"mn" => "Mongolian",
	"mo" => "Moldavian",
	"mr" => "Marathi",
	"ms" => "Malay",
	"mt" => "Maltese",
	"my" => "Burmese",
	"na" => "Nauru",
	"ne" => "Nepali",
	"nl" => "Dutch",
	"no" => "Norwegian",
	"oc" => "Occitan",
	"om" => "(Afan) Oromo",
	"or" => "Oriya",
	"pa" => "Punjabi",
	"pl" => "Polish",
	"ps" => "Pashto / Pushto",
	"pt" => "Portuguese",
	"qu" => "Quechua",
	"rm" => "Rhaeto-Romance",
	"rn" => "Kirundi",
	"ro" => "Romanian",
	"ru" => "Russian",
	"rw" => "Kinyarwanda",
	"sa" => "Sanskrit",
	"sd" => "Sindhi",
	"sg" => "Sangro",
	"sh" => "Serbo-Croatian",
	"si" => "Singhalese",
	"sk" => "Slovak",
	"sl" => "Slovenian",
	"sm" => "Samoan",
	"sn" => "Shona",
	"so" => "Somali",
	"sq" => "Albanian",
	"sr" => "Serbian",
	"ss" => "Siswati",
	"st" => "Sesotho",
	"su" => "Sundanese",
	"sv" => "Swedish",
	"sw" => "Swahili",
	"ta" => "Tamil",
	"te" => "Tegulu",
	"tg" => "Tajik",
	"th" => "Thai",
	"ti" => "Tigrinya",
	"tk" => "Turkmen",
	"tl" => "Tagalog",
	"tn" => "Setswana",
	"to" => "Tonga",
	"tr" => "Turkish",
	"ts" => "Tsonga",
	"tt" => "Tatar",
	"tw" => "Twi",
	"ug" => "Uigur",
	"uk" => "Ukrainian",
	"ur" => "Urdu",
	"uz" => "Uzbek",
	"vi" => "Vietnamese",
	"vo" => "Volapuk",
	"wo" => "Wolof",
	"xh" => "Xhosa",
	"yi" => "Yiddish",
	"yo" => "Yoruba",
	"za" => "Zuang",
	"zh" => "Chinese",
	"zu" => "Zulu"
);
ossn_register_languages('hi', $hi);
