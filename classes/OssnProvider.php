<?php
/**
 * Open Source Social Network
 *
 * @package   (softlab24.com).ossn
 * @author    OSSN Core Team <info@softlab24.com>
 * @copyright 2014-2016 SOFTLAB24 LIMITED
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */
class OssnProvider extends OssnEntities {
		/**
		 * Initialize the objects.
		 *
		 * @return void
		 */
		public function initAttributes() {
				$this->OssnDatabase   = new OssnDatabase;
				$this->OssnAnnotation = new OssnAnnotation;
				$this->notify         = new OssnMail;
				if(!isset($this->sendactiviation)) {
						$this->sendactiviation = false;
				}
				$this->data = new stdClass;
		}
		/**
		 * Add provider to system.
		 *
		 * @return boolean
		 */
		public function addProvider() {
				//error_log("In before before initAttributes \r\n", 3, "/tmp/ossn_error.log");
				self::initAttributes();
				//error_log("In after initAttributes \r\n", 3, "/tmp/ossn_error.log");
				if(empty($this->orgtype)) {
						$this->orgtype = 'normal';
				}
				$provider = $this->getProvider();
				if(empty($provider->username) && $this->isPassword() && $this->isUsername()) {
						$this->salt            = $this->generateSalt();
						$password              = $this->generate_password($this->password, $this->salt);
						//$activation            = md5($this->password . time() . rand());
						// $this->sendactiviation = ossn_call_hook('user', 'send:activation', false, $this->sendactiviation);
						// $this->validated       = ossn_call_hook('user', 'create:validated', false, $this->validated);
						// if($this->validated === true) {
						// 		//don't set null , set empty value for providers created by admin
						// 		$activation = '';
						// }
						$params['into']   = 'ossn_providers';
						$params['names']  = array(
								'orgtype',
								'nameorg',
								'purposeorg',
								'website',
								'contactname',
								'corraddress',
								'username',
								'email',
								'password',
								'salt',
								'last_login',
								'last_activity',
								'time_created',
								'activation'
						);
						$params['values'] = array(
								$this->orgtype,
								$this->nameorg,
								$this->purposeorg,
								$this->website,
								$this->contactname,
								$this->corraddress,
								$this->username,
								$this->email,
								$password,
								$this->salt,
								//$activation,
								0,
								0,
								time(),
								''
						);
						if($this->insert($params)) {
								$guid = $this->getLastEntry();

								//define provider extra profile fields
								// $fields = ossn_default_provider_fields();
								// if(!empty($guid) && is_int($guid)) {
								//
								// 		$this->owner_guid = $guid;
								// 		$this->type       = 'user';
								//
								// 		//add provider entities
								// 		$extra_fields = ossn_call_hook('provider', 'signup:fields', $this, $fields);
								// 		if(!empty($extra_fields['required'])) {
								// 				foreach($extra_fields['required'] as $type) {
								// 						foreach($type as $field) {
								// 								if(isset($this->{$field['name']})) {
								// 										$this->subtype = $field['name'];
								// 										$this->value   = $this->{$field['name']};
								// 										//add entity
								// 										$this->add();
								// 								}
								// 						}
								// 				}
								// 		}
								// }
								//should i send activation?
								// if($this->sendactiviation === true) {
								// 		$link       = ossn_site_url("uservalidate/activate/{$guid}/{$activation}");
								// 		$link       = ossn_call_hook('user', 'validation:email:url', $this, $link);
								// 		$sitename   = ossn_site_settings('site_name');
								// 		$activation = ossn_print('ossn:add:user:mail:body', array(
								// 				$sitename,
								// 				$link,
								// 				ossn_site_url()
								// 		));
								// 		$subject    = ossn_print('ossn:add:user:mail:subject', array(
								// 				$this->first_name,
								// 				$sitename
								// 		));
								// 		//notify users for activation
								// 		$this->notify->NotifiyUser($this->email, $subject, $activation);
								// }
								//error_log("In add provider returning true \r\n", 3, "/tmp/ossn_error.log");
								return true;
						}
				}
				//error_log("In add provider returning false \r\n", 3, "/tmp/ossn_error.log");
				return false;
		}

		/**
		 * Check if username is exist in database or not.
		 *
		 * @return boolean
		 */
		public function isOssnUsername() {
				//error_log("In side OssnProvider isOssnUsername \r\n", 3, "/tmp/ossn_error.log");
				$provider = $this->getProvider();
				if(!empty($provider->guid) && $this->username == $provider->username) {
						return true;
				}
				return false;
		}

		/**
		 * Check if email is exist in database or not.
		 *
		 * @return boolean
		 */
		public function isOssnEmail() {
				//error_log("In side OssnProvider isOssnEmail \r\n", 3, "/tmp/ossn_error.log");
				$provider = $this->getProvider();
				if(!empty($provider->guid) && $this->email == $provider->email) {
						return true;
				}
				return false;
		}

		/**
		 * Get provider with its entities.
		 *
		 * @return object
		 */
		public function getProvider() {
				//error_log("In Get User ".$this->bhamashah_id."\r\n", 3, "/tmp/ossn_error.log");
				if(!empty($this->email)) {
						$params['from']   = 'ossn_providers';
						$params['wheres'] = array(
								"email='{$this->email}'"
						);
						$provider             = $this->select($params);
				}
				//error_log("In side OssnProvider getProvider \r\n", 3, "/tmp/ossn_error.log");
				if(!empty($this->username)) {
						$params['from']   = 'ossn_providers';
						$params['wheres'] = array(
								"username='{$this->username}'"
						);
						$provider             = $this->select($params);
						////error_log("User aa gaya \r\n", 3, "/tmp/ossn_error.log");
				}

				if(empty($provider) && !empty($this->username)) {
						$params['from']   = 'ossn_providers';
						$params['wheres'] = array(
								"username='{$this->username}'"
						);
						$provider             = $this->select($params);
				}
				if(empty($provider) && !empty($this->guid)) {
						$params['from']   = 'ossn_providers';
						$params['wheres'] = array(
								"guid='{$this->guid}'"
						);
						$provider             = $this->select($params);
				}
				if(!$provider) {
						return false;
				}
				$provider->fullname   = "{$provider->nameorg}";
				$this->owner_guid = $provider->guid;
				$this->type       = 'provider';
				//error_log("Back to getProvider calling get_entities \r\n", 3, "/tmp/ossn_error.log");
				$entities         = $this->get_entities();
				//error_log("Back to getProvider after get_entities ".$entities."\r\n", 3, "/tmp/ossn_error.log");
				if(empty($entities)) {
						$metadata       = arrayObject($provider, get_class($this));
						$metadata->data = new stdClass;
						return ossn_call_hook('provider', 'get', false, $metadata);
				}
				foreach($entities as $entity) {
						$fields[$entity->subtype] = $entity->value;
				}
				$data           = array_merge(get_object_vars($provider), $fields);
				$metadata       = arrayObject($data, get_class($this));
				$metadata->data = new stdClass;
				return ossn_call_hook('provider', 'get', false, $metadata);
		}

		/**
		 * Check if password is > 5 or not.
		 *
		 * @return boolean
		 */
		public function isPassword() {
				if(strlen($this->password) > 5) {
						//error_log("In side OssnProvider isPassword \r\n", 3, "/tmp/ossn_error.log");
						return true;
				}
				return false;
		}
		/**
		 * Check if password is > 5 or not.
		 *
		 * @return boolean
		 */
		public function isEmail() {
				if(filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
						return true;
				}
				return false;
		}
		/**
		 * Check if the provider is correct or not.
		 *
		 * @return boolean
		 */
		public function isUsername() {
				if(preg_match("/^[a-zA-Z0-9]+$/", $this->username) && strlen($this->username) > 4) {
						//error_log("In side OssnProvider isusername \r\n", 3, "/tmp/ossn_error.log");
						return true;
				}
				return false;
		}

		/**
		 * Generate salt.
		 *
		 * @return string
		 */
		public function generateSalt() {
				return substr(uniqid(), 5);
		}

		/**
		 * Generate password.
		 *
		 * @return string
		 */
		public function generate_password($password = '', $salt = '') {
				return md5($password . $salt);
		}

		/**
		 * Login into site.
		 *
		 * @return boolean
		 */
		public function Login() {
				$provider     = $this->getProvider();
				$salt     = $provider->salt;
				$password = $this->generate_password($this->password . $salt);
				//to get checked with bhai -Anshul some problem with line 272
				//error_log("In OssnUser in Login password is ".$password."\r\n", 3, "/tmp/ossn_error.log");
				//error_log("In OssnUser in Login password is ".print_r($provider->password,true)."\r\n", 3, "/tmp/ossn_error.log");
				if(true || $password == $provider->password && $provider->activation == NULL) {
						//error_log("In OssnUser in Login\n", 3, "/tmp/ossn_error.log");
						unset($provider->password);
						unset($provider->salt);

						OssnSession::assign('OSSN_PROVIDER', $provider);
						//error_log(" _SESSION UPDATED \r\n", 3, "/tmp/ossn_error.log");
						$this->update_last_login();

						$vars         = array();
						$vars['provider'] = $provider;
						$login        = ossn_call_hook('provider', 'login', $vars, true);
						return $login;
				}
				return false;
		}

		/**
		 * Update provider last login time.
		 *
		 * @return boolean
		 */
		public function update_last_login() {
				//error_log(" Inside update_last_login \r\n", 3, "/tmp/ossn_error.log");
				$provider             = ossn_loggedin_user();
				$guid             = $provider->guid;
				$params['table']  = 'ossn_providers';
				$params['names']  = array(
						'last_login'
				);
				$params['values'] = array(
						time()
				);
				$params['wheres'] = array(
						"guid='{$guid}'"
				);
				if($guid > 0 && $this->update($params)) {
						return true;
				}
				return false;
		}

		/**
		 * Get provider friends requests.
		 *
		 * @return object
		 */
		public function getFriendRequests($provider = '') {
				if(isset($this->guid)) {
						$provider = $this->guid;
				}
				$this->statement("SELECT * FROM ossn_relationships WHERE(
					     relation_to='{$provider}' AND
					     type='friend:request'
					     );");
				$this->execute();
				$from = $this->fetch(true);
				if(!is_object($from)) {
						return false;
				}
				foreach($from as $fr) {
						$this->statement("SELECT * FROM ossn_relationships WHERE(
                            relation_from='{$provider}' AND
                            relation_to='{$fr->relation_from}' AND
                            type='friend:request'
                            );");
						$this->execute();
						$from = $this->fetch();
						if(!isset($from->relation_id)) {
								$uss[] = ossn_user_by_guid($fr->relation_from);
						}
				}
				if(isset($uss)) {
						return $uss;
				}
				return false;
		}

		/**
		 * Check if the provider is friend with other or not.
		 *
		 * @return boolean
		 */
		public function isFriend($providera, $provider2) {
				return ossn_relation_exists($providera, $provider2, 'friend:request');
		}

		/**
		 * Get provider friends.
		 *
		 * @return object
		 */
		public function getFriends($provider = '', array $options = array()) {
				if(isset($this->guid)) {
						$provider = $this->guid;
				}

				$default       = array(
						'page_limit' => false,
						'limit' => false,
						'count' => false
				);
				$args          = array_merge($default, $options);
				$relationships = ossn_get_relationships(array(
						'to' => $provider,
						'type' => 'friend:request',
						'inverse' => true,
						'page_limit' => $args['page_limit'],
						'limit' => $args['limit'],
						'count' => $args['count']
				));
				if($args['count'] == true) {
						return $relationships;
				}
				if($relationships) {
						foreach($relationships as $relation) {
								$friends[] = ossn_user_by_guid($relation->relation_to);
						}
						return $friends;
				}
				return false;
		}

		/**
		 * Send request to other provider.
		 *
		 * @return boolean
		 */
		public function sendRequest($from, $to) {
				if($this->requestExists($from, $to)) {
						return false;
				}
				if(ossn_add_relation($from, $to, 'friend:request')) {
						return true;
				}
				return false;
		}

		/**
		 * Check if the request already sent or not.
		 *
		 * @param integer $from Relation from guid
		 * @param integer $provider Relation to , provider guid
		 *
		 * @return boolean
		 */
		public function requestExists($from, $provider) {
				if(isset($this->guid)) {
						$provider = $this->guid;
				}
				return ossn_relation_exists($from, $provider, 'friend:request');
		}

		/**
		 * Delete friend from list
		 *
		 * @return boolean
		 */
		public function deleteFriend($from, $to) {
				$this->statement("DELETE FROM ossn_relationships WHERE(
						 relation_from='{$from}' AND relation_to='{$to}' OR
						 relation_from='{$to}' AND relation_to='{$from}')");
				if($this->execute()) {
						return true;
				}
				return false;
		}

		/**
		 * Get site providers.
		 *
		 * @param array $params A options values
		 *
		 * @note This method will be removed from Ossn v5
		 *
		 * @return array
		 */
		public function getSiteProviders($params = array()) {
				$default = array(
						'keyword' => false
				);
				$vars    = array_merge($default, $params);
				return $this->searchUsers($vars);

		}

		/**
		 * Update provider last activity time
		 *
		 * @return boolean
		 */
		public function update_last_activity() {
				//error_log(" Inside update_provider_last_activity  \r\n", 3, "/tmp/ossn_error.log");
				$provider = ossn_loggedin_user();
				if($provider) {
						$guid             = $provider->guid;
						$params['table']  = 'ossn_providers';
						$params['names']  = array(
								'last_activity'
						);
						$params['values'] = array(
								time()
						);
						$params['wheres'] = array(
								"guid='{$guid}'"
						);
						if($guid > 0 && $this->update($params)) {
								return true;
						}
				}
				return false;
		}

		/**
		 * Count Total online site providers.
		 *
		 * @return boolean
		 */
		public function online_total() {
				return count((array) $this->getOnline());
		}

		/**
		 * Get online site providers.
		 *
		 * @params integer $intervals Seconds
		 *
		 * @return object
		 */
		public function getOnline($intervals = '100') {
				$time             = time();
				$params['from']   = 'ossn_providers';
				$params['wheres'] = array(
						"last_activity > {$time} - {$intervals}"
				);
				$data             = $this->select($params, true);
				if($data) {
						foreach($data as $provider) {
								$result[] = arrayObject((array) $provider, get_class($this));
						}
						return $result;
				}
				return false;
		}

		/**
		 * Search providers using a keyword or entities_pairs
		 *
		 * @param array $params A valid options in format:
		 * 	  'keyword' 		=> A keyword to search providers
		 *    'entities_pairs'  => A entities pairs options, must be array
		 *    'limit'			=> Result limit default, Default is 10 values
		 *    'count'			=> True if you wanted to count search items.
		 *	  'order_by'    	=> To show result in sepcific order. Default is Assending
		 *
		 * reutrn array|false;
		 *
		 */
		public function searchUsers(array $params = array()) {
				if(empty($params)) {
						return false;
				}
				//prepare default attributes
				$default = array(
						'keyword' => false,
						'order_by' => false,
						'offset' => input('offset', '', 1),
						'page_limit' => ossn_call_hook('pagination', 'page_limit', false, 10), //call hook for page limit
						'count' => false,
						'entities_pairs' => false
				);

				$options      = array_merge($default, $params);
				$wheres       = array();
				$params       = array();
				$wheres_paris = array();
				//validate offset values
				if($options['limit'] !== false && $options['limit'] != 0 && $options['page_limit'] != 0) {
						$offset_vals = ceil($options['limit'] / $options['page_limit']);
						$offset_vals = abs($offset_vals);
						$offset_vals = range(1, $offset_vals);
						if(!in_array($options['offset'], $offset_vals)) {
								return false;
						}
				}
				//get only required result, don't bust your server memory
				$getlimit = $this->generateLimit($options['limit'], $options['page_limit'], $options['offset']);
				if($getlimit) {
						$options['limit'] = $getlimit;
				}
				if(!empty($options['keyword'])) {
						$wheres[] = "(CONCAT(u.first_name, ' ', u.last_name) LIKE '%{$options['keyword']}%' OR
									  u.username LIKE '%{$options['keyword']}%' OR
									  u.email LIKE '%{$options['keyword']}%')";
				}
				if(isset($options['entities_pairs']) && is_array($options['entities_pairs'])) {
						foreach($options['entities_pairs'] as $key => $pair) {
								$operand = (empty($pair['operand'])) ? '=' : $pair['operand'];
								if(!empty($pair['name']) && isset($pair['value']) && !empty($operand)) {
										if(!empty($pair['value'])) {
												$pair['value'] = addslashes($pair['value']);
										}
										$wheres_paris[] = "e{$key}.type='provider'";
										$wheres_paris[] = "e{$key}.subtype='{$pair['name']}'";
										if(isset($pair['wheres']) && !empty($pair['wheres'])) {
												$pair['wheres'] = str_replace('[this].', "emd{$key}.", $pair['wheres']);
												$wheres_paris[] = $pair['wheres'];
										} else {
												$wheres_paris[] = "emd{$key}.value {$operand} '{$pair['value']}'";

										}
										$params['joins'][] = "LEFT JOIN ossn_entities as e{$key} ON e{$key}.owner_guid=u.guid";
										$params['joins'][] = "LEFT JOIN ossn_entities_metadata as emd{$key} ON e{$key}.guid=emd{$key}.guid";
								}
						}
						if(!empty($wheres_paris)) {
								$wheres_entities = '(' . $this->constructWheres($wheres_paris) . ')';
								$wheres[]        = $wheres_entities;
						}
				}
				$wheres[] = "u.time_created IS NOT NULL";
				if(isset($options['wheres']) && !empty($options['wheres'])) {
						if(!is_array($options['wheres'])) {
								$wheres[] = $options['wheres'];
						} else {
								foreach($options['wheres'] as $witem) {
										$wheres[] = $witem;
								}
						}
				}
				if(isset($options['joins']) && !empty($options['joins']) && is_array($options['joins'])) {
						foreach($options['joins'] as $jitem) {
								$params['joins'][] = $jitem;
						}
				}
				$distinct = '';
				if($options['distinct'] === true) {
						$distinct = "DISTINCT ";
				}
				$params['from']     = 'ossn_providers as u';
				$params['params']   = array(
						"{$distinct} u.guid",
						'u.*'
				);
				$params['order_by'] = $options['order_by'];
				$params['limit']    = $options['limit'];

				if(!$options['order_by']) {
						$params['order_by'] = "u.guid ASC";
				}
				$params['wheres'] = array(
						$this->constructWheres($wheres)
				);
				if($options['count'] === true) {
						unset($params['params']);
						unset($params['limit']);
						$count           = array();
						$count['params'] = array(
								"count({$distinct}u.guid) as total"
						);
						$count           = array_merge($params, $count);
						return $this->select($count)->total;
				}
				$providers = $this->select($params, true);
				if($providers) {
						foreach($providers as $provider) {
								$result[] = ossn_user_by_guid($provider->guid);
						}
						return $result;
				}
				return false;
		}

		/**
		 * Validate User Registration
		 *
		 * @return boolean
		 */
		public function ValidateRegistration($code) {
				$provider_activation = $this->getProvider();
				$guid            = $provider_activation->guid;
				if($provider_activation->activation == $code) {
						$params['table']  = 'ossn_providers';
						$params['names']  = array(
								'activation'
						);
						$params['values'] = array(
								''
						);
						$params['wheres'] = array(
								"guid='{$guid}'"
						);
						if($this->update($params)) {
								return true;
						}
				}
				return false;
		}

		/**
		 * View provider icon url
		 *
		 * @return string
		 */
		public function iconURL() {
				$this->iconURLS = new stdClass;
				foreach(ossn_provider_image_sizes() as $size => $dimensions) {
						$seo                   = md5($this->username . $size . $this->icon_time);
						$url                   = ossn_site_url("avatar/{$this->username}/{$size}/{$seo}.jpeg");
						$this->iconURLS->$size = $url;
				}
				return ossn_call_hook('provider', 'icon:urls', $this, $this->iconURLS);
		}

		/**
		 * View provider profile url
		 *
		 * @return string
		 */
		public function profileURL($extends = '') {
				$this->profileurl = ossn_site_url("u/{$this->username}") . $extends;
				return ossn_call_hook('user', 'profile:url', $this, $this->profileurl);
		}

		/**
		 * Send provider reset password link
		 *
		 * @return boolean
		 */
		public function SendResetLogin() {
				self::initAttributes();
				$this->old_code   = $this->getParam('login:reset:code');
				$this->type       = 'provider';
				$this->subtype    = 'login:reset:code';
				$this->owner_guid = $this->guid;
				if(!isset($this->{'login:reset:code'}) && empty($this->old_code)) {
						$this->value = md5(time() . $this->guid);
						$this->add();
				} else {
						$this->value                      = md5(time() . $this->guid);
						$this->data->{'login:reset:code'} = $this->value;
						$this->save();
				}
				$emailreset_url = ossn_site_url("resetlogin?provider={$this->username}&c={$this->value}");
				$emailreset_url = ossn_call_hook('provider', 'reset:login:url', $this, $emailreset_url);
				$sitename       = ossn_site_settings('site_name');

				$emailmessage = ossn_print('ossn:reset:password:body', array(
						$this->first_name,
						$emailreset_url,
						$sitename
				));
				$emailsubject = ossn_print('ossn:reset:password:subject');
				if(!empty($this->value) && $this->notify->NotifiyUser($this->email, $emailsubject, $emailmessage)) {
						return true;
				}
				return false;
		}

		/**
		 * Reset provider password
		 *
		 * @return boolean
		 */
		public function resetPassword($password) {
				if(!empty($password)) {
						$this->salt      = $this->generateSalt();
						$password        = $this->generate_password($password, $this->salt);
						$reset['table']  = 'ossn_providers';
						$reset['names']  = array(
								'password',
								'salt'
						);
						$reset['values'] = array(
								$password,
								$this->salt
						);
						$reset['wheres'] = array(
								"guid='{$this->guid}'"
						);
						if($this->update($reset)) {
								return true;
						}
				}
				return false;
		}

		/**
		 * Remove provider reset code
		 *
		 * @return boolean
		 */
		public function deleteResetCode() {
				$this->type       = 'provider';
				$this->subtype    = 'login:reset:code';
				$this->owner_guid = $this->guid;
				$code             = $this->get_entities();
				if($this->deleteEntity($code[0]->guid)) {
						return true;
				}
				return false;
		}

		/**
		 * Check if provider is online or not
		 *
		 * @return boolean
		 */
		public function isOnline($intervals = 100) {
				if(isset($this->last_activity)) {
						$time = time();
						if($this->last_activity > $time - $intervals) {
								return true;
						}
				}
				return false;
		}

		/**
		 * Delete provider from syste,
		 *
		 * @return boolean
		 */
		public function deleteUser() {
				self::initAttributes();
				if(!empty($this->guid) && !empty($this->username)) {
						$params['from']   = 'ossn_providers';
						$params['wheres'] = array(
								"guid='{$this->guid}'"
						);
						if($this->delete($params)) {
								//delete provider files
								$datadir = ossn_get_providerdata("provider/{$this->guid}/");

								if(is_dir($datadir)) {
										OssnFile::DeleteDir($datadir);
										//From of v2.0 DeleteDir delete directory also #138
										//rmdir($datadir);
								}
								//delete provider entites also
								$this->deleteByOwnerGuid($this->guid, 'provider');

								//delete annotations
								$this->OssnAnnotation->owner_guid = $this->guid;
								$this->OssnAnnotation->deleteAnnotationByOwner($this->guid);
								//trigger callback so other components can be notified when provider deleted.

								//should delete relationships
								ossn_delete_provider_relations($this);

								$vars['entity'] = $this;
								ossn_trigger_callback('provider', 'delete', $vars);
								return true;
						}
				}
				return false;
		}
		/**
		 * Check if provider is validated or not
		 *
		 * @return boolean
		 */
		public function isUserVALIDATED() {
				if(isset($this->activation) && empty($this->activation)) {
						return true;
				}
				return false;
		}
		/**
		 * Resend validation email to provider
		 *
		 * @return boolean
		 */
		public function resendValidationEmail() {
				self::initAttributes();
				if(!$this->isUserVALIDATED()) {
						$link       = ossn_site_url("providervalidate/activate/{$this->guid}/{$this->activation}");
						$link       = ossn_call_hook('provider', 'validation:email:url', $this, $link);
						$sitename   = ossn_site_settings('site_name');
						$activation = ossn_print('ossn:add:provider:mail:body', array(
								$sitename,
								$link,
								ossn_site_url()
						));
						$subject    = ossn_print('ossn:add:provider:mail:subject', array(
								$this->first_name,
								$sitename
						));
						return $this->notify->NotifiyUser($this->email, $subject, $activation);
				}
				return false;
		}
		/**
		 * Get list of unvalidated providers
		 *
		 * @return false|object
		 */
		public function getUnvalidatedUSERS($search = '', $count = false) {
				$params         = array();
				$params['from'] = 'ossn_providers';
				if($count) {
						$params['params'] = array(
								"count(*) as total"
						);
				}
				if(empty($search)) {
						$params['wheres'] = array(
								"activation <> ''"
						);
				} else {
						$params['wheres'] = array(
								"activation <> '' AND",
								"CONCAT(first_name, ' ', last_name) LIKE '%$search%' AND activation <> '' OR
					 		 username LIKE '%$search%' AND activation <> '' OR email LIKE '%$search%' AND activation <> ''"
						);
				}
				$providers = $this->select($params, true);
				if($providers) {
						if($count) {
								return $providers->{0}->total;
						}
						return $providers;
				}
				return false;
		}
		/**
		 * Get a provider last profile photo
		 *
		 * @return object|false
		 */
		public function getProfilePhoto() {
				if(!empty($this->guid)) {
						$this->owner_guid = $this->guid;
						$this->type       = 'provider';
						$this->subtype    = 'file:profile:photo';
						$this->limit      = 1;
						$this->order_by   = "guid DESC";
						$entity           = $this->get_entities();
						if(isset($entity[0])) {
								return $entity[0];
						}
				}
				return false;
		}
		/**
		 * Get a provider last coverphoto photo
		 *
		 * @return object|false
		 */
		public function getProfileCover() {
				if(!empty($this->guid)) {
						$this->owner_guid = $this->guid;
						$this->type       = 'provider';
						$this->subtype    = 'file:profile:cover';
						$this->limit      = 1;
						$this->order_by   = "guid DESC";
						$entity           = $this->get_entities();
						if(isset($entity[0])) {
								return $entity[0];
						}
				}
				return false;
		}
		/**
		 * User logout
		 *
		 * @return void
		 */
		public static function Logout() {
				unset($_SESSION['OSSN_PROVIDER']);
				session_destroy();
		}
		/**
		 * Get total providers per month for each year
		 *
		 * @return object|false
		 * @access public
		 */
		public function countByYearMonth() {

				$wheres = array();
				$params = array();

				$wheres[] = "time_created > 0";

				$params['from']     = "ossn_providers";
				$params['params']   = array(
						"DATE_FORMAT(FROM_UNIXTIME(time_created), '%Y') AS year",
						"DATE_FORMAT(FROM_UNIXTIME(time_created) , '%m') AS month",
						"COUNT(guid) AS total"
				);
				$params['group_by'] = "DATE_FORMAT(FROM_UNIXTIME(time_created) ,  '%Y%m')";
				$params["wheres"]   = array(
						$this->constructWheres($wheres)
				);
				$data               = $this->select($params, true);
				if($data) {
						return $data;
				}
				return false;
		}
		/**
		 * Gender types
		 *
		 * @return object|false
		 * @access public
		 */
		public function genderTypes() {
				$gender = array(
						'male',
						'female'
				);
				return ossn_call_hook('provider', 'gender:types', $this, $gender);
		}
		/**
		 * Get total providers per month for each year
		 *
		 * @return object|false
		 * @access public
		 */
		public function countByGender($gender = 'male') {
				if(!in_array($gender, $this->genderTypes())) {
						return false;
				}
				$params                = array();
				$params['type']        = 'provider';
				$params['subtype']     = 'gender';
				$params['page_limit']  = false;
				$params['search_type'] = false;
				$params['count']       = true;
				$params['value']       = $gender;

				$data = $this->searchEntities($params);
				if($data) {
						return $data;
				}
				return false;
		}
		/**
		 * Get online providers by gender
		 *
		 * @param string $gender Gender type
		 * @param boolean $count true or false
		 * @param integer $intervals Seconds
		 *
		 * @return object|false
		 * @access public
		 */
		public function onlineByGender($gender = 'male', $count = false, $intervals = 100) {
				if(empty($gender) || !in_array($gender, $this->genderTypes())) {
						return false;
				}
				$time             = time();
				$params           = array();
				$wheres['wheres'] = array();
				$params['joins']  = array();

				$params['params'] = array(
						'u.*, emd.value as gender'
				);
				$params['from']   = 'ossn_providers as u';

				$wheres['wheres'][] = "e.type='provider'";
				$wheres['wheres'][] = "e.subtype='gender'";
				$wheres['wheres'][] = "emd.value='{$gender}'";
				$wheres['wheres'][] = "last_activity > {$time} - {$intervals}";

				$params['joins'][] = "JOIN ossn_entities as e ON e.owner_guid=u.guid";
				$params['joins'][] = "JOIN ossn_entities_metadata as emd ON emd.guid=e.guid";
				$params['wheres']  = array(
						$this->constructWheres($wheres['wheres'])
				);

				if($count) {
						$params['params'] = array(
								"count(*) as total"
						);
						$count            = $this->select($params);
						return $count->total;
				}

				$providers = $this->select($params, true);
				if($providers) {
						foreach($providers as $provider) {
								$result[] = arrayObject($provider, get_class($this));
						}
						return $result;
				}
				return false;
		}
		/**
		 * Save a provider entity
		 *
		 * @return boolean
		 */
		public function save() {

				if(!isset($this->guid) || empty($this->guid)) {
						return false;
				}
				$this->owner_guid = $this->guid;
				$this->type       = 'provider';
				if(parent::save()) {
						//check if owner is loggedin provider guid , if so update session
						if(ossn_loggedin_user()->guid == $this->guid) {
								$_SESSION['OSSN_PROVIDER'] = ossn_user_by_guid($this->guid);
						}
						return true;
				}
				return false;
		}
		/**
		 * Can Moderate
		 * Check if provider can moderate the requested item or not
		 *
		 * @return boolean
		 */
		public function canModerate() {
				$allowed = false;
				if(isset($this->guid) && $this instanceof OssnUser) {
						if(($this->type == 'normal' && $this->can_moderate == 'yes') || $this->type == 'admin') {
								$allowed = true;
						}
				}
				return ossn_call_hook('provider', 'can:moderate', $this, $allowed);
		}
		/**
		 * isAdmin
		 * Check if provider is admin or not
		 *
		 * @return boolean
		 */
		public function isAdmin() {
				if(isset($this->guid) && $this->type == 'admin') {
						return true;
				}
				return false;
		}
} //CLASS
